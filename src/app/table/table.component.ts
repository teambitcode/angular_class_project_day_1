import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
tableName:string = "My Student Data";
age:number = 10;
studentData:Student[]  = [
  {
    name:"SSSSS",
    subject:"IT", 
    marks: 90
  },
  {
    name:"BBBBB",
    subject:"English",
    marks: 80
  },
  {
    name:"LLLLL",
    subject:"IT",
    marks: 20
  },
  {
    name:"GGGGGG",
    subject:"English",
    marks: 30
  },
  {
    name:"KKKKK",
    subject:"Maths",
    marks: 50
  }]

  constructor() { }

  ngOnInit(): void {
    
  }

}


class Student{
  name:string;
  subject:string;
  marks:number;
}
